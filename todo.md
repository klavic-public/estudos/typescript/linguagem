https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/void
# Tópicos a documentar.

## 1 Variáveis

### 1.1 Declaração de variáveis

[ ] var
[ ] let
[ ] const

### Tipos de variáveis - 

[ ] number
[ ] boolean
[ ] string
[ ] date
[ ] null
[ ] undefined
[ ] Array
[ ] Object
[ ] void
[ ] never
[ ] unknown
[ ] function

## 2 Estrutura condicional

[ ] if.
[ ] else.
[ ] else if.
[ ] switch - case.

## Loops
[ ] - For
[ ] - Do/While
[ ] - While
[ ] - For/in
[ ] - For/of
[ ] - ForEach
[ ] - Map
[ ] - Filter
[ ] - Reduce

## Operadores
[ ] Adição: "+".
[ ] Adição atribuição: "+=".
[ ] Operador ternário (ternary operator): "?".
[ ] Decremento: "--".
[ ] Delete: "delete".
[ ] Atribuição por desconstrução (Destructuring assignment).
[ ] Divisão: "/".
[ ] Divisão atribuição: "/=".
[ ] Igualdade:  "==" e "===".
[ ] Exponenciação: "**".
[ ] Exponenciação atribuição: "**=".
[ ] Maior: ">".
[ ] Maior igual: ">=".
[ ] operador in: "in".
[ ] incremento: "++"
[ ] Diferença: "!=" e "!=="
[ ] instanceof: "instanceof"
[ ] Menor: "<"
[ ] Menor igual: "<="
[ ] AND: "&&"
[ ] AND atribuição: "&&=";
[ ] Negação (NOT): "!"
[ ] OR: "||"
[ ] OR atribuição: "||="
[ ] Multiplicação: "*"
[ ] Multiplicação atribuição: "*="
[ ] Nullish coalescing: "??"
[ ] Nullish coalescing Atribuição: "??="
[ ] Opcional chain;
[ ] Resto: "%"
[ ] Resto atribuição: "%="
[ ] Spreed = "..."
[ ] Substração = "-"
[ ] Substração atribuição = "-="
[ ] typeof

# Outros
[ ] Clause guards
[ ] Callbacks
[ ] Closures.

# Orientação objeto
[ ] Classes
[ ] Herança
[ ] Abstração
[ ] Sobrecarga
[ ] Sobrescrever. 

## Typescript

[ ] - Union.
[ ] - Interfaces.
[ ] - Type Assertions.
[ ] - Literal Types.
[ ] - Enums
[ ] - Generic.
[ ] - Rest parameters.